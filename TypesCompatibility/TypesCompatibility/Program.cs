﻿using System;
using TypesCompatibility.Models;

namespace TypesCompatibility
{
	class Program
	{
		static void Main(string[] args)
		{
			Random rnd = new Random();
			Figure[] figures = new Figure[10];
			for (int i = 0; i < figures.Length; i++)
			{
				int type = rnd.Next(2);
				switch(type)
				{
					case 0: 
						figures[i] = new Circle();
						break;
					case 1:
						figures[i] = new Rectangle();
						break;
				}
			}

			foreach (var figure in figures)
			{
				Console.WriteLine($"{figure.X} {figure.Y} {figure.GetType()}");
			}
		}
	}
}
