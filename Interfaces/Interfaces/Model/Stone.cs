﻿using System;

namespace Interfaces.Model
{
	public class Stone : BaseObject
	{
		public override void Draw()
		{
			Console.WriteLine($"Stone: {X} {Y}");
		}
	}
}
