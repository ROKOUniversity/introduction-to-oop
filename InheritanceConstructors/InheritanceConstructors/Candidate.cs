﻿namespace Inheritance
{
	class Candidate : Person
	{
		public Candidate(string firstName, string lastName)
			:base(firstName, lastName)
		{

		}

		public int VacancyId { get; set; }
		public VacancyState State { get; set; }
	}
}
