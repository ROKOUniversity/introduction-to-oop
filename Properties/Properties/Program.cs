﻿using System;

namespace Properties
{
	class Program
	{
		static void Main(string[] args)
		{
			HeadPhones headPhones = new HeadPhones();

			headPhones.Volume = 10;
			headPhones.Volume += 10;

			Console.WriteLine(headPhones.Volume);

			headPhones.Step = 10;
			headPhones.Increase();

			Console.WriteLine(headPhones.Volume);

			//MinimizeVolumeStep(ref headPhones.Step);
			headPhones.Increase();

			Console.WriteLine(headPhones.Volume);
		}
		public static void MinimizeVolumeStep(ref int value)
		{
			value = 1;
		}
	}
}
