﻿using System;

namespace BasicConsoleGame.GameObjects
{
	public class Door : GameObject, IPermeable
	{
		public override void Draw()
		{
			Console.ForegroundColor = ConsoleColor.Green;
			Console.SetCursorPosition(X, Y);
			Console.Write((char)9619);
		}
	}
}
