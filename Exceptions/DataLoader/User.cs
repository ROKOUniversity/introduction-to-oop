﻿using System;

namespace DataLoader
{
	public class User
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public DateTime Birthday { get; set; }
		public double Weight { get; set; }
		public int Height { get; set; }
	}
}
