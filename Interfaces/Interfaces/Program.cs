﻿using System;
using Interfaces.Model;

namespace Interfaces
{
	class Program
	{
		static void Main(string[] args)
		{
			Random rnd = new Random();
			BaseObject[] objects = new BaseObject[10];

			int movableObjectCount = 0;
			for (int i = 0; i < objects.Length; i++)
			{
				int type = rnd.Next(3);
				switch (type)
				{
					case 0:
						objects[i] = new Stone();
						break;
					case 1:
						objects[i] = new Bear();
						movableObjectCount++;
						break;
					case 2:
						objects[i] = new Wolf();
						movableObjectCount++;
						break;
				}
				objects[i].X = rnd.Next(100);
				objects[i].Y = rnd.Next(100);
			}

			IMovable[] movableObjects = new IMovable[movableObjectCount];
			int count = 0;
			for (int i = 0; i < objects.Length; i++)
			{
				if (objects[i] is IMovable)
				{
					movableObjects[count] = objects[i] as IMovable;
					count++;
				}
			}

			foreach (var obj in objects)
			{
				obj.Draw();
			}
			Console.WriteLine();

			foreach (var obj in movableObjects)
			{
				obj.Move();
			}

			foreach (var obj in objects)
			{
				obj.Draw();
			}
		}
	}
}
