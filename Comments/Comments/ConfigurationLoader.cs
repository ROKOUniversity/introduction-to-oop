﻿using System;
using System.IO;

namespace ConstReadonly
{
	static class ConfigurationLoader
	{
		private const string ConfigName = "config.txt";

		private static readonly Configuration configuration;

		public static Configuration Configuration { get { return configuration; } }

		static ConfigurationLoader()
		{
			string fullPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigName);
			string[] options = File.ReadAllLines(fullPath);

			configuration = new Configuration(
				reportName: GetSetting("ReportName", options),
				writeToLog: bool.Parse(GetSetting("WriteToLog", options))
			);
		}

		private static string GetSetting(string key, string[] options)
		{
			string result = null;
			for (int i = 0; i < options.Length; i++)
			{
				if(options[i].StartsWith(key + ":"))
				{
					return options[i].Substring(key.Length + 1);
				}
			}
			return result;
		}
	}
}
