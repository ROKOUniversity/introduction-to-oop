﻿namespace ConstReadonly
{
	static class PhysicsHelper
	{
		public const int C = 299792458;
		public const double G = 6.67384e-11;
		public const double h = 6.62607015e-34;
	}
}
