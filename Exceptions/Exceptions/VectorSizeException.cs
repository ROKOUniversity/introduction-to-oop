﻿using System;

namespace Exceptions
{
	public class VectorSizeException : Exception
	{
		public VectorSizeException()
		{
		}

		public VectorSizeException(string message)
			:base(message)
		{
		}
	}
}
