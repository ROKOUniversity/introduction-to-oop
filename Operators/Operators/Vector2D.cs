﻿using System;

namespace Operators
{
	public class Vector2D
	{
		public double X { get; set; }
		public double Y { get; set; }

		public double Length { get { return Math.Sqrt(X * X + Y * Y); } }

		public static Vector2D operator - (Vector2D v)
		{
			return new Vector2D() { X = -v.X, Y = -v.Y };
		}

		public static Vector2D operator + (Vector2D first, Vector2D second)
		{
			return new Vector2D() { X = first.X + second.X, Y = first.Y + second.Y };
		}

		public static Vector2D operator - (Vector2D first, Vector2D second)
		{
			return new Vector2D() { X = first.X - second.X, Y = first.Y - second.Y };
		}

		public static Vector2D operator * (Vector2D v, double scalar)
		{
			return new Vector2D() { X = v.X * scalar, Y = v.Y * scalar };
		}

		public static Vector2D operator * (double scalar, Vector2D v)
		{
			return v * scalar;
		}

		public static Vector2D operator / (Vector2D v, double scalar)
		{
			return new Vector2D() { X = v.X / scalar, Y = v.Y / scalar };
		}

		public static bool operator == (Vector2D v1, Vector2D v2)
		{
			return v1.X == v2.X && v1.Y == v2.Y;
		}

		public static bool operator != (Vector2D v1, Vector2D v2)
		{
			return v1.X != v2.X || v1.Y != v2.Y;
		}

		public override string ToString()
		{
			return $"({X}, {Y})";
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(this, obj))
			{
				return true;
			}

			if (ReferenceEquals(obj, null))
			{
				return false;
			}

			Vector2D v = obj as Vector2D;
			if (v == null)
			{
				return false;
			}

			return X == v.X && Y == v.Y;
		}

		public override int GetHashCode()
		{
			return X.GetHashCode() ^ Y.GetHashCode();
		}
	}
}
