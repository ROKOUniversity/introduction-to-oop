﻿using System;

namespace ArrayHelper
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Input array length");
			int n = int.Parse(Console.ReadLine());

			int[] arr = ArrayUtils.GenerateRandomArray(n);
			ArrayUtils.WriteArray(arr);
			int sum = ArrayUtils.Summa(arr);
			int max = ArrayUtils.Max(arr);

			//ArrayUtils util = new ArrayUtils();

			Console.WriteLine($"Summa = {sum}");
			Console.WriteLine($"Maximum = {max}");
		}
	}
}
