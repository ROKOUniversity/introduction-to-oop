﻿using ExternalClassLibrary;

namespace SealedClasses
{
	public class Ring : Circle
	{
		public override string GetDrawMessage()
		{
			return "Ring";
		}
	}
}
