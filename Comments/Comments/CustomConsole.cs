﻿using System;
using System.IO;

namespace ConstReadonly
{
	/// <summary>
	/// Custom console supports dubbing message writing to log file
	/// </summary>
	class CustomConsole
	{
		private readonly Configuration configuration;

		/// <summary>
		/// Configurate your custom console uses config argument. 
		/// Set config.WriteToLog=true to support file logging and config.ReportName=filename
		/// </summary>
		public CustomConsole(Configuration config)
		{
			configuration = config;
		}

		/// <summary>
		/// Method write message to the console and to a log file, configured in the constructor
		/// </summary>
		/// <param name="message">Text message</param>
		public void WriteLine(string message)
		{
			Console.WriteLine(message);
			if(configuration.WriteToLog)
			{
				File.AppendAllLines(configuration.ReportName, new string[] { DateTime.Now + ": " + message });
			}
		}
	}
}
