﻿namespace Inheritance
{
	class Person
	{
		public string FirstName { get; set; }
		
		public string LastName { get; set; }

		protected string email;
		public string Email 
		{
			get { return email; } 
			set
			{
				if (value.Contains("@") && value.Contains("."))
					email = value;
			}
		}

		public string Phone { get; set; }
	}
}
