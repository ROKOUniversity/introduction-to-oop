﻿namespace Interfaces.Model
{
	public class Bear : BaseObject, IMovable
	{
		public override void Draw()
		{
			System.Console.WriteLine($"Bear: {X} {Y}");
		}

		public void Move()
		{
			X += 1;
			Y += 1;
		}
	}
}
