﻿using System;

namespace Rectangles
{
	class Program
	{
		static void Main(string[] args)
		{
			Rectangle rect1 = new Rectangle();
			rect1.width = 5;
			rect1.height = 6;

			Rectangle rect2 = new Rectangle();
			rect2.width = 10;
			rect2.height = 8;

			Console.WriteLine(rect1.GetArea());
			Console.WriteLine(rect2.GetArea());
		}
	}
}
