﻿namespace Statics
{
	public class HeadPhones
	{
		private int volume;

		public static int Step { get; set; } = 5;

		//static HeadPhones()
		//{
		//	Step = 5;
		//}

		public int Volume
		{
			get 
			{ 
				return volume;
			}
			set 
			{
				if (value >= 0 && value <= 100)
				{
					volume = value;
				}
			}
		}

		public int GetVolume()
		{
			return volume;
		}

		public void SetVolume(int value)
		{
			if (value >= 0 && value <= 100)
			{
				volume = value;
			}
		}

		public void Increase()
		{
			if (volume + Step <= 100)
				volume += Step;
		}

		public void Decrease()
		{
			if (volume - Step >= 0)
				volume -= Step;
		}
	}
}
