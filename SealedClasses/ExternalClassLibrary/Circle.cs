﻿namespace ExternalClassLibrary
{
	public class Circle : Figure
	{
		public override string GetDrawMessage()
		{
			return "Circle";
		}
	}
}
