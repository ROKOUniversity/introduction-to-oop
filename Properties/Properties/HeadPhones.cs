﻿namespace Properties
{
	public class HeadPhones
	{
		private int volume;

		public int Volume
		{
			get 
			{ 
				return volume;
			}
			set 
			{
				if (value >= 0 && value <= 100)
				{
					volume = value;
				}
			}
		}

		//public int GetVolume()
		//{
		//	return volume;
		//}

		//public void SetVolume(int value)
		//{
		//	if (value >= 0 && value <= 100)
		//	{
		//		volume = value;
		//	}
		//}

		public int Step { get; set; } = 5;

		public void Increase()
		{
			if (volume + Step <= 100)
				volume += Step;
		}

		public void Decrease()
		{
			if (volume - Step >= 0)
				volume -= Step;
		}
	}
}
