﻿using System;

namespace DataLoader
{
	public class ManyErrorsException : Exception
	{
		public int ErrorCount { get; private set; }

		public ManyErrorsException(int errorCount)
		{
			ErrorCount = errorCount;
		}

		public ManyErrorsException(string message, int errorCount)
			: base(message)
		{
			ErrorCount = errorCount;
		}
	}
}
