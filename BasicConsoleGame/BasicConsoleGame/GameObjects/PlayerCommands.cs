﻿namespace BasicConsoleGame.GameObjects
{
	public enum PlayerCommands
	{
		None = 0,
		Up = 1,
		Down = 2,
		Left = 3,
		Right = 4
	}
}
