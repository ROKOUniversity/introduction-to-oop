﻿using System;

namespace Inheritance
{
	class Person
	{
		public Person (string firstName, string lastName)
		{
			Console.WriteLine("Person (string firstName, string lastName)");
			FirstName = firstName;
			LastName = lastName;
		}

		public string FirstName { get; set; }
		
		public string LastName { get; set; }

		protected string email;
		public string Email 
		{
			get { return email; } 
			set
			{
				if (value.Contains("@") && value.Contains("."))
					email = value;
			}
		}

		public string Phone { get; set; }
	}
}
