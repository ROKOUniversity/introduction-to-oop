﻿using System;

namespace NetClasses
{
	class Program
	{
		static void Main(string[] args)
		{
			Circle c = new Circle() { R = 1 };

			object o = c;
			int x = 5;
			Console.WriteLine("x = {0}", x);

			Console.WriteLine(c);
			Console.WriteLine(c.ToString());
			Console.WriteLine(c.GetType());
		}
	}
}
