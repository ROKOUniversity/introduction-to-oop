﻿using System;

namespace Interfaces.Model
{
	public class Wolf : BaseObject, IMovable
	{
		Random rnd = new Random();
		public override void Draw()
		{
			Console.WriteLine($"Wolf {X} {Y}");
		}

		public void Move()
		{
			X += rnd.Next(2) - 1;
			Y += rnd.Next(2) - 1;
		}
	}
}
