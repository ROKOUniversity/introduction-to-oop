﻿namespace ConstReadonly
{
	class Configuration
	{
		public Configuration(bool writeToLog, string reportName)
		{
			ReportName = reportName;
			WriteToLog = writeToLog;
		}

		public string ReportName { get; }
		public bool WriteToLog { get; }
	}
}
