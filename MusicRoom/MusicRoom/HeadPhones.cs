﻿namespace MusicRoom
{
	public class HeadPhones
	{
		private int volume;
		private int step = 5;

		public int GetVolume()
		{
			return volume;
		}

		public void Increase()
		{
			if (volume + step <= 100)
				volume += step;
		}

		public void Decrease()
		{
			if (volume - step >= 0)
				volume -= step;
		}
	}
}
