﻿using System;

namespace BasicConsoleGame.GameObjects.Obstacles
{
	public class Stone : GameObject
	{
		public override void Draw()
		{
			Console.ForegroundColor = ConsoleColor.Gray;
			Console.SetCursorPosition(X, Y);
			Console.Write((char)8962);
		}
	}
}
