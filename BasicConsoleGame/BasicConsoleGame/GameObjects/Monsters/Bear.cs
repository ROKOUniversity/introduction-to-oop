﻿using System;

namespace BasicConsoleGame.GameObjects.Monsters
{
	public class Bear : MovableObject
	{
		private readonly Random rnd = new Random();

		public override void Draw()
		{
			if (_oldX != 0 && _oldY != 0)
			{
				Console.SetCursorPosition(_oldX, _oldY);
				Console.Write(' ');
			}

			Console.ForegroundColor = ConsoleColor.Red;
			Console.SetCursorPosition(X, Y);
			Console.Write((char)9650);
		}

		protected override void ChangePosition()
		{
			if(Math.Abs(Map.Player.X - X) + Math.Abs(Map.Player.Y - Y) < 5)
			{
				X += Map.Player.X - X > 0 ? 1 : -1;
				Y += Map.Player.Y - Y > 0 ? 1 : -1;
			}
			else
			{
				X += rnd.Next(3) - 1;
				Y += rnd.Next(3) - 1;
			}
		}
	}
}
