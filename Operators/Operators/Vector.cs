﻿namespace Operators
{
	public class Vector
	{
		public int Size { get; private set; }
		private double[] values;

		public Vector(int size)
		{
			Size = size;
			values = new double[size];
		}

		public double this[int n]
		{
			get { return values[n]; }
			set { values[n] = value;  }
		}

		public static Vector operator + (Vector first, Vector second)
		{
			Vector result = new Vector(first.Size);
			for (int i = 0; i < result.Size; i++)
			{
				result[i] = first[i] + second[i];
			}
			return result;
		}

		public override string ToString()
		{
			// bad code. Please, use StringBuilder in real code;
			string result = "(";
			for (int i = 0; i < Size; i++)
			{
				result += values[i];
				if(i < Size - 1)
					result += ", ";
			}
			result += ")";
			return result;
		}

		public static implicit operator Vector(Vector2D v)
		{
			Vector vector = new Vector(2);
			vector[0] = v.X;
			vector[1] = v.Y;
			return vector;
		}

		public static explicit operator Vector2D(Vector v)
		{
			Vector2D vector = new Vector2D();
			vector.X = v[0];
			vector.Y = v[1];
			return vector;
		}
	}
}
