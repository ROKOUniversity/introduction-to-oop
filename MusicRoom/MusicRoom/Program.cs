﻿using System;

namespace MusicRoom
{
	class Program
	{
		static void Main(string[] args)
		{
			HeadPhones headPhones = new HeadPhones();

			ConsoleKeyInfo keyInfo;
			do
			{
				Console.SetCursorPosition(0, 0);
				Console.WriteLine($"Current volume: {headPhones.GetVolume()}  ");
				keyInfo = Console.ReadKey();
				switch (keyInfo.Key)
				{
					case ConsoleKey.UpArrow:
						headPhones.Increase(); break;
					case ConsoleKey.DownArrow:
						headPhones.Decrease(); break;
				}
			} while (keyInfo.Key != ConsoleKey.Escape);
		}
	}
}
