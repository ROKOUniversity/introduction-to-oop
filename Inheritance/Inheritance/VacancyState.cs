﻿namespace Inheritance
{
	enum VacancyState
	{
		New,
		InterviewCompleted,
		JobOfferSent
	}
}
