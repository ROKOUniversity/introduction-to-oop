﻿using System;

namespace BasicConsoleGame.GameObjects.Monsters
{
	public class Hare : MovableObject
	{
		private readonly Random rnd = new Random();

		public override void Draw()
		{
			if (_oldX != 0 && _oldY != 0)
			{
				Console.SetCursorPosition(_oldX, _oldY);
				Console.Write(' ');
			}

			Console.ForegroundColor = ConsoleColor.White;
			Console.SetCursorPosition(X, Y);
			Console.Write((char)8728);
		}

		protected override void ChangePosition()
		{
			X += rnd.Next(3) - 1;
			Y += rnd.Next(3) - 1;
		}
	}
}
