﻿namespace StructsAndClasses
{
	struct SRectangle
	{
		public int width;
		public int height;

		public int GetArea()
		{
			return width * height;
		}
	}
}
