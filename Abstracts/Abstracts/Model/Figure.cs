﻿namespace TypeConversion.Model
{
	public abstract class Figure
	{
		public abstract double GetArea();
		public int X { get; set; }
		public int Y { get; set; }
	}
}
