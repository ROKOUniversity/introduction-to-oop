﻿using System;
using System.Diagnostics;
using System.IO;

namespace DataLoader
{
	public class DataParser
	{
		public User[] LoadUsers(string path)
		{
			string[] dataStrings = File.ReadAllLines(path);

			User[] users = new User[dataStrings.Length - 1];
			int errorCount = 0;

			for (int i = 1; i < dataStrings.Length; i++)
			{
				string[] data = dataStrings[i].Split(',');
				User user;
				try
				{
					 user = new User()
					 {
						 FirstName = data[0],
						 LastName = data[1],
						 Birthday = DateTime.Parse(data[2]),
						 Height = int.Parse(data[3]),
						 Weight = double.Parse(data[4])
					 };
					users[i - 1] = user;
				}
				catch(FormatException ex)
				{
					Debug.WriteLine($"Error in {i}, \"{dataStrings[i]}\", {ex.Message}");
					errorCount++;
				}
			}

			if (errorCount > dataStrings.Length / 10)
				throw new ManyErrorsException(errorCount);

			return users;
		}
	}
}
