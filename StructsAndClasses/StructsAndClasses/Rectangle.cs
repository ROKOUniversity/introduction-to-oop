﻿namespace StructsAndClasses
{
	class Rectangle
	{
		public int width;
		public int height;

		public int GetArea()
		{
			return width * height;
		}
	}
}