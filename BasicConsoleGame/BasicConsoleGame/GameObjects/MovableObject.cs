﻿namespace BasicConsoleGame.GameObjects
{
	public abstract class MovableObject : GameObject
	{
		protected int _oldX;
		protected int _oldY;

		public Map Map { get; set; }

		public virtual void Move()
		{
			_oldX = X;
			_oldY = Y;

			ChangePosition();

			if (!Map.CanMove(X, Y) || !CanMove())
			{
				X = _oldX;
				Y = _oldY;
			}
		}

		protected abstract void ChangePosition();

		protected virtual bool CanMove()
		{
			foreach (var obj in Map.GameObjects)
			{
				if (obj != this && obj is not IPermeable && obj.X == X && obj.Y == Y)
				{
					return false;
				}
			}
			return true;
		}
	}
}
