﻿namespace TypesCompatibility.Models
{
	public class Rectangle : Figure
	{
		public int Width { get; set; }
		public int Height { get; set; }
	}
}
