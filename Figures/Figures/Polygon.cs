﻿using System;

namespace Figures
{
	public class Polygon
	{
		public Point[] points;
		public double GetPerimeter()
		{
			if (points.Length <= 1)
				return 0;

			double perimeter = 0;
			for (int i = 1; i < points.Length; i++)
			{
				perimeter += Math.Sqrt((points[i].x - points[i-1].x) * 
					(points[i].x - points[i - 1].x) + 
					(points[i].y - points[i - 1].y) * 
					(points[i].y - points[i - 1].y));
			}

			perimeter += Math.Sqrt((points[0].x - points[points.Length - 1].x) *
				(points[0].x - points[points.Length - 1].x) +
				(points[0].y - points[points.Length - 1].y) *
				(points[0].y - points[points.Length - 1].y));

			return perimeter;
		}
	}
}
