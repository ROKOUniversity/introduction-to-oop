﻿using System;

namespace NetClasses
{
	public class Circle : Figure
	{
		public int X { get; set; }
		public int Y { get; set; }
		public double R { get; set; }

		public sealed override void Draw()
		{
			Console.WriteLine("Circle");
		}

		public double GetLength() { return 2 * Math.PI * R; }

		public override string ToString()
		{
			return $"({X}, {Y}): {R}";
		}
	}
}
