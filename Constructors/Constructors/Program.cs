﻿using System;

namespace Figures
{
	class Program
	{
		static void Main(string[] args)
		{
			Polygon square = new Polygon(4);			

			square.points[0].x = 0;
			square.points[0].y = 0;
			square.points[1].x = 0;
			square.points[1].y = 2;
			square.points[2].x = 2;
			square.points[2].y = 2;
			square.points[3].x = 2;
			square.points[3].y = 0;

			Polygon copy = new Polygon(square);

			square.points[0].x = 1;

			Console.WriteLine($"Perimeter = {square.GetPerimeter()}");
			Console.WriteLine($"Perimeter = {copy.GetPerimeter()}");
		}
	}
}
