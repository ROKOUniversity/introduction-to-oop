﻿using System;

namespace Inheritance
{
	class Program
	{
		static void Main(string[] args)
		{
			Person p = new Person()
			{
				FirstName = "Ivan",
				LastName = "Ivanov",
				Email = "ivan@mail.ru",
				Phone = "123456"
			};

			Employee e = new Employee()
			{
				FirstName = "Petr",
				LastName = "Petrov",
				Email = "test@test",
				Phone = "5323213",
				DepartmenyId = 1,
				Room = 408
			};

			Candidate c = new Candidate()
			{
				FirstName = "Sidor",
				LastName = "Sidorov",
				Email = "sidorov@gmailcom",
				Phone = "123456",
				VacancyId = 13,
				State = VacancyState.New
			};

			Console.WriteLine($"Person: {p.FirstName} {p.LastName} {p.Email}");
			Console.WriteLine($"Employee: {e.FirstName} {e.LastName} {e.Room}");
			Console.WriteLine($"Candidate: {c.FirstName} {c.LastName} {c.State}");
		}
	}
}
