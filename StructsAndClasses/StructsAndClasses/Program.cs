﻿using System;

namespace StructsAndClasses
{
	class Program
	{
		static void Main(string[] args)
		{
			Rectangle rect;
			rect = new Rectangle();

			rect.width = 5;
			rect.height = 6;

			SRectangle srect;

			srect.width = 7;
			srect.height = 8;

			UpdateRectangle(rect);
			UpdateRectangle(srect);

			Console.WriteLine($"Class: {rect.width} {rect.height}");
			Console.WriteLine($"Struct: {srect.width} {srect.height}");
		}

		static void UpdateRectangle(Rectangle urect)
		{
			urect.width = 10;
		}

		static void UpdateRectangle(SRectangle usrect)
		{
			usrect.width = 10;
		}
	}
}
