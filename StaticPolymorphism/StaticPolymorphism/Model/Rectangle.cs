﻿namespace TypeConversion.Model
{
	public class Rectangle : Figure
	{
		public int Width { get; set; }
		public int Height { get; set; }
		public new double GetArea() { return Width * Height; }
	}
}
