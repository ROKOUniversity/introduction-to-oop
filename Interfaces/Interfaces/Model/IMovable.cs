﻿namespace Interfaces.Model
{
	public interface IMovable
	{
		void Move();
	}
}