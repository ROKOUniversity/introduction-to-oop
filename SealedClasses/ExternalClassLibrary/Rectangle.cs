﻿namespace ExternalClassLibrary
{
	public sealed class Rectangle : Figure
	{
		public sealed override string GetDrawMessage()
		{
			return "Rectangle";
		}
	}
}
