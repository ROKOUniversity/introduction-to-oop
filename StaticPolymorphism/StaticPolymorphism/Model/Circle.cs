﻿using System;

namespace TypeConversion.Model
{
	public class Circle : Figure
	{
		public double R { get; set; }
		public double GetLength() { return 2 * Math.PI * R; }
		public new double GetArea() { return Math.PI * R * R; }
	}
}
