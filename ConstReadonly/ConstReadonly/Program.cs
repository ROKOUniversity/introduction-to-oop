﻿using System;

namespace ConstReadonly
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine(Math.Sin(2 * MathHelper.PI));
			MathHelper.PI = 4;
			Console.WriteLine(Math.Sin(2 * Math.PI));

			Console.WriteLine($"Light speed: {PhysicsHelper.C}");

			CustomConsole console = new(ConfigurationLoader.Configuration);

			console.WriteLine("Hello!");
        }
	}
}
