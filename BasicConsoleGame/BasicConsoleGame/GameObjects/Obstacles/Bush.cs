﻿using System;

namespace BasicConsoleGame.GameObjects.Obstacles
{
	public class Bush : GameObject, IPermeable
	{
		public override void Draw()
		{
			Console.ForegroundColor = ConsoleColor.Green;
			Console.SetCursorPosition(X, Y);
			Console.Write((char)9827);
		}
	}
}
