﻿using System;

namespace Figures
{
	class Program
	{
		static void Main(string[] args)
		{
			Circle c = new Circle();
			c.center = new Point();
			c.center.x = 5;
			c.center.y = 6;
			c.radius = 10;

			Console.WriteLine($"Length = {c.GetLength()}");

			Polygon polygon = new Polygon();
			polygon.points = new Point[4];
			polygon.points[0] = new Point();
			polygon.points[1] = new Point();
			polygon.points[2] = new Point();
			polygon.points[3] = new Point();

			polygon.points[0].x = 0;
			polygon.points[0].y = 0;
			polygon.points[1].x = 0;
			polygon.points[1].y = 2;
			polygon.points[2].x = 2;
			polygon.points[2].y = 2;
			polygon.points[3].x = 2;
			polygon.points[3].y = 0;

			Console.WriteLine($"Perimeter = {polygon.GetPerimeter()}");
		}
	}
}
