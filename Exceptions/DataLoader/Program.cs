﻿using System;
using System.IO;

namespace DataLoader
{
	class Program
	{
		static void Main(string[] args)
		{
			DataParser parser = new DataParser();

			User[] users = null;
			try
			{
				users = parser.LoadUsers(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "file1.csv"));
			}
			catch(ManyErrorsException ex)
			{
				Console.WriteLine($"There are many errors ({ex.ErrorCount}) in file. Please, check file format and culture.");
			}
			catch(Exception ex)
			{
				Console.WriteLine($"Exception of type {ex.GetType()} was catched. Details: {ex.Message}");
			}

			if (users != null)
			{
				Console.WriteLine(users.Length);
			}
		}
	}
}
