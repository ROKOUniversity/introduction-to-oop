﻿using System;

namespace Operators
{
	class Program
	{
		static void Main(string[] args)
		{
			Vector2D v1 = new Vector2D() { X = 5, Y = 1 };
			Vector2D v2 = new Vector2D() { X = -2, Y = 2 };

			Vector2D sum = v1 + v2;

			Console.WriteLine($"{v1} + {v2} = {sum}");

			Vector x = new Vector(3);
			x[0] = 1;
			x[1] = 2;
			x[2] = 3;
			Vector y = new Vector(3);
			y[0] = 3;
			y[1] = 2;
			y[2] = 1;
			Vector z = x + y;

			Vector v = v1;
			Vector2D v2d = (Vector2D)v;


			Console.WriteLine($"{x} + {y} = {z}");
		}
	}
}
