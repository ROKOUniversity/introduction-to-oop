﻿using BasicConsoleGame.GameObjects;
using BasicConsoleGame.GameObjects.Monsters;
using BasicConsoleGame.GameObjects.Obstacles;
using System;

namespace BasicConsoleGame
{
	class Program
	{
		static void Main(string[] args)
		{
			Random rnd = new Random();
			Map map = new Map() { Width = 40, Height = 10 };
			Player player = new Player() { X = 1, Y = 1 };
			map.Player = player;
			map.Player.Map = map;
			Door door = new Door() { X = 40, Y = 10 };
			map.Door = door;
			
			map.GameObjects = new GameObject[27];
			map.GameObjects[0] = player;
			map.GameObjects[1] = door;

			for (int i = 2; i < 12; i++)
			{
				map.GameObjects[i] = new Stone() { X = rnd.Next(1, map.Width + 1), Y = rnd.Next(1, map.Height + 1) };
			}
			for (int i = 12; i < 22; i++)
			{
				map.GameObjects[i] = new Bush() { X = rnd.Next(1, map.Width + 1), Y = rnd.Next(1, map.Height + 1) };
			}
			for (int i = 22; i < 25; i++)
			{
				map.GameObjects[i] = new Hare() { X = rnd.Next(1, map.Width + 1), Y = rnd.Next(1, map.Height + 1), Map = map };
			}
			for (int i = 25; i < 27; i++)
			{
				map.GameObjects[i] = new Bear() { X = rnd.Next(1, map.Width + 1), Y = rnd.Next(1, map.Height + 1), Map = map };
			}

			map.Draw();

			ConsoleKeyInfo keyInfo = new ConsoleKeyInfo();
			do
			{
				keyInfo = Console.ReadKey();

				PlayerCommands command = PlayerCommands.None;

				switch(keyInfo.Key)
				{
					case ConsoleKey.UpArrow:
					case ConsoleKey.W:
						command = PlayerCommands.Up; 
						break;
					case ConsoleKey.DownArrow:
					case ConsoleKey.S:
						command = PlayerCommands.Down; 
						break;
					case ConsoleKey.LeftArrow:
					case ConsoleKey.A:
						command = PlayerCommands.Left; 
						break;
					case ConsoleKey.RightArrow:
					case ConsoleKey.D:
						command = PlayerCommands.Right; 
						break;
				}

				map.Player.SetCommand(command);

				foreach (var obj in map.GameObjects)
				{
					MovableObject movableObj = obj as MovableObject;
					movableObj?.Move();					
				}

				map.Draw();

				if(IsLoser(map))
				{
					Console.ForegroundColor = ConsoleColor.Red;
					Console.WriteLine("You lose!!!");
					break;
				}

				if(map.Player.X == map.Door.X && map.Player.Y == map.Door.Y)
				{
					Console.ForegroundColor = ConsoleColor.Green;
					Console.WriteLine("You win!!!");
					break;
				}

			} while (keyInfo.Key != ConsoleKey.Escape);
		}

		private static bool IsLoser(Map map)
		{
			foreach (var obj in map.GameObjects)
			{
				Bear bear = obj as Bear;
				if(bear != null)
				{
					if (bear.X == map.Player.X && bear.Y == map.Player.Y)
						return true;
				}
			}
			return false;
		}
	}
}
