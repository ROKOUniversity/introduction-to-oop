﻿namespace Exceptions
{
	public class Vector
	{
		public int Size { get; private set; }
		private double[] values;

		public Vector(int size)
		{
			Size = size;
			values = new double[size];
		}

		public double this[int n]
		{
			get { return values[n]; }
			set { values[n] = value;  }
		}

		public static Vector operator + (Vector first, Vector second)
		{
			if (first.Size != second.Size)
				throw new VectorSizeException($"First vector size is {first.Size}, and the second vector size is {second.Size}");

			Vector result = new Vector(first.Size);
			for (int i = 0; i < result.Size; i++)
			{
				result[i] = first[i] + second[i];
			}
			return result;
		}

		public override string ToString()
		{
			// bad code. Please, use StringBuilder in real code;
			string result = "(";
			for (int i = 0; i < Size; i++)
			{
				result += values[i];
				if(i < Size - 1)
					result += ", ";
			}
			result += ")";
			return result;
		}
	}
}