﻿using System;
using TypeConversion.Model;

namespace TypeConversion
{
	class Program
	{
		static void Main(string[] args)
		{
			Random rnd = new Random();
			Figure[] figures = new Figure[10];
			for (int i = 0; i < figures.Length; i++)
			{
				int type = rnd.Next(2);
				switch (type)
				{
					case 0:
						figures[i] = new Circle() 
						{ 
							X = rnd.Next(100), 
							Y = rnd.Next(100), 
							R = rnd.Next(100) 
						};
						break;
					case 1:
						figures[i] = new Rectangle()
						{
							X = rnd.Next(100),
							Y = rnd.Next(100),
							Width = rnd.Next(100),
							Height = rnd.Next(100)
						};
						break;
				}
			}

			foreach (var figure in figures)
			{
				Circle c = figure as Circle;
				if(c == null)
				{
					Console.WriteLine("Rectangle");
				}
				else
				{
					Console.WriteLine($"{c.X} {c.Y} {c.R} {c.GetType()}");
				}

				//if (figure is Circle)
				//{
				//	Circle c = (Circle)figure;
				//	Console.WriteLine($"{c.X} {c.Y} {c.R} {c.GetType()}");
				//}
				//else if(figure is Rectangle)
				//{
				//	Rectangle r = (Rectangle)figure;
				//	Console.WriteLine($"{r.X} {r.Y} {r.Width} {r.Height} {r.GetType()}");
				//}
			}
		}
	}
}
