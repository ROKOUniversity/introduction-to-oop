﻿using BasicConsoleGame.GameObjects;
using System;

namespace BasicConsoleGame
{
	public class Map
	{
		public int Width { get; set; }
		public int Height { get; set; }

		public GameObject[] GameObjects { get; set; }
		public Player Player { get; set; }
		public Door Door { get; set; }

		public void Draw()
		{
			Console.ForegroundColor = ConsoleColor.Gray;
			for (int i = 0; i <= Width + 1; i++)
			{
				Console.SetCursorPosition(i, 0);
				Console.Write('\u2593');
				Console.SetCursorPosition(i, Height + 1);
				Console.Write('\u2593');
			}

			for (int i = 0; i <= Height + 1; i++)
			{
				Console.SetCursorPosition(0, i);
				Console.Write('\u2593');
				Console.SetCursorPosition(Width + 1, i);
				Console.Write('\u2593');
			}

			for (int i = 0; i < GameObjects.Length; i++)
			{
				GameObjects[i].Draw();
			}

			Console.SetCursorPosition(0, Height + 2);
		}

		public bool CanMove(int x, int y)
		{
			return x > 0 && y > 0 && x <= Width && y <= Height;
		}
	}
}
