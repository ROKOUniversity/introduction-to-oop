﻿using System;

namespace Inheritance
{
	class Employee : Person
	{
		public Employee(string firstName, string lastName)
			: base(firstName, lastName)
		{

		}

		public Employee(string firstName, string lastName, int room)
			: base(firstName, lastName)
		{
			Console.WriteLine("Employee(string firstName, string lastName, int room)"); 
			Room = room;
		}

		public Employee(string firstName, string lastName, int room, int departmentId)
			: this(firstName, lastName, room)
		{
			Console.WriteLine("Employee(string firstName, string lastName, int room, int departmentId)");
			DepartmenyId = departmentId;
		}


		public string Login 
		{
			get { return email; }
		}
		public int DepartmenyId { get; set; }
		public int Room { get; set; }
	}
}
