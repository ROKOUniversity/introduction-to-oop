﻿using System;
using System.IO;

namespace ConstReadonly
{
	class CustomConsole
	{
		private readonly Configuration configuration;
		public CustomConsole(Configuration config)
		{
			configuration = config;
		}

		public void WriteLine(string message)
		{
			Console.WriteLine(message);
			if(configuration.WriteToLog)
			{
				File.AppendAllLines(configuration.ReportName, new string[] { DateTime.Now + ": " + message });
			}
		}
	}
}
