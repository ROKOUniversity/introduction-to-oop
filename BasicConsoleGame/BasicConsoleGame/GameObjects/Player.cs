﻿using System;

namespace BasicConsoleGame.GameObjects
{
	public class Player : MovableObject, IPermeable
	{
		private PlayerCommands _command;

		public override void Draw()
		{
			if (_oldX != 0 && _oldY != 0)
			{
				Console.SetCursorPosition(_oldX, _oldY);
				Console.Write(' ');
			}

			Console.ForegroundColor = ConsoleColor.White;
			Console.SetCursorPosition(X, Y);
			Console.Write((char)9787);
		}

		public void SetCommand(PlayerCommands command)
		{
			_command = command;
		}

		protected override void ChangePosition()
		{
			switch (_command)
			{
				case PlayerCommands.Up: Y--; break;
				case PlayerCommands.Down: Y++; break;
				case PlayerCommands.Left: X--; break;
				case PlayerCommands.Right: X++; break;
			}
			_command = PlayerCommands.None;
		}
	}
}
