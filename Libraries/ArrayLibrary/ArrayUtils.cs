﻿using System;

namespace ArrayLibrary
{
	public static class ArrayUtils
	{
		public static int[] GenerateRandomArray(int n)
		{
			int[] arr = new int[n];

			Random rnd = new Random();

			for (int i = 0; i < arr.Length; i++)
			{
				arr[i] = rnd.Next(-100, -10);
			}

			return arr;
		}

		public static int Max(int[] arr)
		{
			int max = int.MinValue;
			for (int i = 0; i < arr.Length; i++)
			{
				if (max < arr[i])
				{
					max = arr[i];
				}
			}
			return max;
		}

		public static int Summa(int[] arr)
		{
			int sum = 0;

			for (int i = 0; i < arr.Length; i++)
			{
				sum += arr[i];
			}

			return sum;
		}

		public static void WriteArray(int[] arr)
		{
			for (int i = 0; i < arr.Length; i++)
			{
				Console.WriteLine(arr[i]);
			}
		}
	}
}
