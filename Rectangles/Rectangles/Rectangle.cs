﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rectangles
{
	public class Rectangle
	{
		public int width;
		public int height;

		public int GetArea()
		{
			return width * height;
		}
	}
}
