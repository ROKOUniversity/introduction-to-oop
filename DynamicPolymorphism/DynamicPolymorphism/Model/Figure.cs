﻿namespace TypeConversion.Model
{
	public class Figure
	{
		public virtual double GetArea() { return 0; }
		public int X { get; set; }
		public int Y { get; set; }
	}
}
