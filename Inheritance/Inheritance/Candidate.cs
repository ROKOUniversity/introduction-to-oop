﻿namespace Inheritance
{
	class Candidate : Person
	{
		public int VacancyId { get; set; }
		public VacancyState State { get; set; }
	}
}
