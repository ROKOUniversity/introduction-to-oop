﻿using System;

namespace Figures
{
	public class Circle
	{
		public Point center;
		public int radius;

		public double GetLength()
		{
			return 2 * Math.PI * radius;
		}
	}
}
