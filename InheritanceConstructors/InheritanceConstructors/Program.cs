﻿using System;

namespace Inheritance
{
	class Program
	{
		static void Main(string[] args)
		{
			Person p = new Person("Ivan", "Ivanov")
			{
				Email = "ivan@mail.ru",
				Phone = "123456"
			};

			Employee e = new Employee("Petr", "Petrov", 100, 2)
			{
				Email = "test@test",
				Phone = "5323213",
				DepartmenyId = 1,
				Room = 408
			};

			Candidate c = new Candidate("Sidor", "Sidorov")
			{
				Email = "sidorov@gmailcom",
				Phone = "123456",
				VacancyId = 13,
				State = VacancyState.New
			};

			Console.WriteLine($"Person: {p.FirstName} {p.LastName} {p.Email}");
			Console.WriteLine($"Employee: {e.FirstName} {e.LastName} {e.Room}");
			Console.WriteLine($"Candidate: {c.FirstName} {c.LastName} {c.State}");
		}
	}
}
