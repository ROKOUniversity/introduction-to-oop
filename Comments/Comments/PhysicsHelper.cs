﻿namespace ConstReadonly
{
	/// <summary>
	/// Physics helper provide some popular physical constants
	/// </summary>
	static class PhysicsHelper
	{
		/// <summary>
		/// Speed of light in vacuum
		/// </summary>
		public const int C = 299792458;

		/// <summary>
		/// Newtonian constant of gravitation
		/// </summary>
		public const double G = 6.67384e-11;

		/// <summary>
		/// Planck constant
		/// </summary>
		public const double h = 6.62607015e-34;
	}
}
