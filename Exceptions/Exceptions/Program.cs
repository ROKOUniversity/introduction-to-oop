﻿using System;

namespace Exceptions
{
	class Program
	{
		static void Main(string[] args)
		{
			int x;

			//try
			//{
			//	x = int.Parse(Console.ReadLine());
			//	Console.WriteLine($"Your input: {x}");
			//}
			//catch
			//{
			//	Console.WriteLine("Something went wrong");
			//}


			Vector v1 = new Vector(3);
			Vector v2 = new Vector(5);
			try
			{
				Vector v = v1 + v2;
				Console.WriteLine(v);
			}
			catch(VectorSizeException ex)
			{
				Console.WriteLine(ex.Message);
			}
		}
	}
}
