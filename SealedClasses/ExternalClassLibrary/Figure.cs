﻿namespace ExternalClassLibrary
{
	public abstract class Figure
	{
		public int X { get; set; }
		public int Y { get; set; }

		public abstract string GetDrawMessage();
	}
}
