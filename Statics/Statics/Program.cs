﻿using System;

namespace Statics
{
	class Program
	{
		static void Main(string[] args)
		{
			HeadPhones headPhones = new()
			{
				Volume = 10
			};

			HeadPhones.Step = 10;

			headPhones.Increase();

			HeadPhones headPhones1 = new()
			{
				Volume = 10
			};

			HeadPhones.Step = 5;

			headPhones1.Increase();
			headPhones.Increase();

		}
	}
}
