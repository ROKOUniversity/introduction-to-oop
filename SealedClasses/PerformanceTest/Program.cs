﻿using ExternalClassLibrary;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace PerformanceTest
{
	class Program
	{
		static void Main(string[] args)
		{
			int count = 1001;
			List<long> resultsCircle = new List<long>(count);
			List<long> resultsRectangle = new List<long>(count);
			for (int i = 0; i < count; i++)
			{
				Stopwatch sw = new Stopwatch();
				sw.Start();

				long x = 0;

				for(int j = 0; j < 1000000; j++)
				{
					Circle c = new Circle();
					var res = c.GetDrawMessage();
					x += res.Length;
				}

				sw.Stop();
				resultsCircle.Add(sw.ElapsedTicks);


				Stopwatch sw1 = new Stopwatch();
				sw1.Start();

				for (int j = 0; j < 1000000; j++)
				{
					Rectangle r = new Rectangle();
					var res = r.GetDrawMessage();
					x += res.Length;
				}

				sw1.Stop();
				resultsRectangle.Add(sw1.ElapsedTicks);

			}

			resultsCircle = resultsCircle.OrderBy(p => p).ToList();
			Console.WriteLine($"Without sealed: {resultsCircle[count / 2 + 1]}");
			resultsRectangle = resultsRectangle.OrderBy(p => p).ToList();
			Console.WriteLine($"With sealed: {resultsRectangle[count / 2 + 1]}");
		}
	}
}
