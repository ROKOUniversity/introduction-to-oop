﻿namespace Interfaces.Model
{
	public abstract class BaseObject
	{
		public int X { get; set; }
		public int Y { get; set; }
		public abstract void Draw();
	}
}
