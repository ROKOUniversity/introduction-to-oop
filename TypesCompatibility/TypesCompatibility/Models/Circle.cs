﻿using System;

namespace TypesCompatibility.Models
{
	public class Circle : Figure
	{
		public double R { get; set; }
		public double GetLength() { return 2 * Math.PI * R; }
	}
}
